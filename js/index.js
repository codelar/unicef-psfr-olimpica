document.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed")

  const swiperImpact = new Swiper(".swiper-container--impact", {
    slidesPerView: 1,
    spaceBetween: 16,
    grabCursor: true,
    pagination: {
      el: ".swiper-pagination--impact",
      clickable: true,
    },
    breakpoints: {
      575: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 24,
        centeredSlides: true,
      },
      992: {
        centeredSlides: false,
        slidesPerView: 3,
      },
    }
  })

  const swiperPractices = new Swiper(".swiper-container--practices", {
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: "auto",
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 100,
      modifier: 2,
      slideShadows: false,
    },
    pagination: {
      el: ".swiper-pagination--practices",
      clickable: true,
    },
  })
})
